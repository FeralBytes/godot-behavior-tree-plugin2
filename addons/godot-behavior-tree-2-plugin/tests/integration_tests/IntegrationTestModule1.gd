extends "res://addons/godot-behavior-tree-2-plugin/tests/TheIntegrationTester.gd"

var tests = [
        "test_1",
    ]

var TestRunner

func setup():
    
    .setup()  # super

func teardown():
    .teardown()  # super

func tests(): 
     for test in tests:
        test_is_running = true  # set here or the while will = false.
        call_deferred(test)
        while test_is_running:
            yield(get_tree(), "idle_frame")
     .tests()  # super

func test_1():
    testcase("IntegrationTestModule: Performs the following test, open_nodes, last_return status, and makes sure the tree is capable of Waiting.")
    TestRunner = get_node("/root/TestRunnerControl")
    var test1 = load("res://addons/godot-behavior-tree-2-plugin/tests/integration_tests/open_nodes_test/OpenNodesTest1.tscn")
    TestRunner.add_child(test1.instance())
    var OpenNodeTest = TestRunner.get_node("OpenNodesTest1")
    assert_equal(OpenNodeTest.last_return, null, "last_return should start null.")
    var context = OpenNodeTest.context 
    var length = len(context.open_nodes)
    assert_equal(length, 0, "The number of open_nodes should be 0.")
    yield(get_tree(), "idle_frame")
    length = len(context.open_nodes)
    assert_equal(length, 2, "The number of open_nodes should be 2.")
    assert_equal(OpenNodeTest.last_return, 44, "last_return should be 44 which is waiting or ERR_BUSY.")
    yield(get_tree(), "idle_frame")
    length = len(context.open_nodes)
    assert_equal(length, 1, "The number of open_nodes should be 1.")
    assert_equal(OpenNodeTest.last_return, 44, "last_return should be 44 which is waiting or ERR_BUSY.")
    yield(get_tree(), "idle_frame")
    length = len(context.open_nodes)
    assert_equal(length, 0, "The number of open_nodes should be 0.")
    assert_equal(OpenNodeTest.last_return, 0, "last_return should be 0 which is success or OK.")
    OpenNodeTest.queue_free()
    yield(get_tree(), "idle_frame")
    endcase()