extends "res://addons/godot-behavior-tree-2-plugin/tests/TheIntegrationTester.gd"

var tests = [
        "test_1_test",
    ]
var test_var

func setup():
    .setup()  # super

func teardown():
    pass
    .teardown()  # super

func tests(): 
     for test in tests:
        test_is_running = true  # set here or the while will = false.
        call_deferred(test)
        while test_is_running:
            yield(get_tree(), "idle_frame")
     .tests()  # super

func test_1_test():
    testcase("ExampleTestIntegrationModule: True is True test.")
    print("test_var = " + str(test_var))
    assert_equal(true, true, "True is True test")
    endcase()

# Notice the use of the word support here to make it clear that it is not a test.
func test_1_support():
    test_var = true