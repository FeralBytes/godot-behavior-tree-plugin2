extends "res://addons/godot-behavior-tree-2-plugin/BehaviorTreeBase.gd"

var counter = 0
# Leaf Node
#warning-ignore:unused_variable
func iteration(context):
    if context.debug:
        print("In Behavior Tree Action.")
    counter += 1
    if counter == 3:
        counter = 0
        return FAILED
    else:
        return ERR_BUSY
