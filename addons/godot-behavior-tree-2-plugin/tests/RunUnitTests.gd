extends Node

var the_tester = load("res://addons/godot-behavior-tree-2-plugin/tests/TheUnitTester.gd")

func _ready():
    # Just in case we crash set the status code in advance to a failure code.
    OS.set_exit_code(70)
    print()
    print("[STARTING ALL UNIT TESTS]")
    the_tester.run([
        "res://addons/godot-behavior-tree-2-plugin/tests/unit_tests/ExampleUnitTestModule.gd",
    ])
    call_deferred("next_test")
    
func next_test():
    get_tree().call_group("TestRunners", "TR_next_test")
