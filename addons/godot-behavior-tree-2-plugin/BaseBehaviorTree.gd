extends Node

var iterations = 0

func _execute(context):

    var status = _iteration(context)

    if status != ERR_BUSY:
        _close(context)

    return status
    
func _open(context):
    iterations = 0
    context.open_node(self)
    
func _iteration(context):
    iterations += 1
    return iteration(context)
    
func _close(context):
    context.close_node(self)
     
#the following functions are to be overridden in extending nodes

#ignore-warning:unused_variable
func open(context):
    pass

#ignore-warning:unused_variable
func iteration(context):
    return OK

#ignore-warning:unused_variable
func close(context):
    pass
