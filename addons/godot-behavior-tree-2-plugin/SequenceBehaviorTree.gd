extends "res://addons/godot-behavior-tree-2-plugin/BaseBehaviorTree.gd"
# OK = 0 ERR_BUSY = 44 FAILED = 1
# Declare member variables here. Examples:
var current_child = null
var last_return = null
onready var child_count = get_child_count()

# Sequence Compsite Node - iterates children until one returns FAILED
#   succeeds ONLY if all children succeed (return OK)
func iteration(context):
    if context.debug:
        print("In Behavior Tree Sequence. Last child return was = " + str(last_return) + " Current child is = " + str(current_child))
    if last_return == FAILED:
        current_child = null
        last_return = null
        return FAILED
    elif last_return == ERR_BUSY:
        last_return = get_child(current_child)._execute(context)
        return ERR_BUSY
    if current_child == null:
        if child_count == 0:
            return OK
        else:
            current_child = 0
            get_child(current_child)._open(context)
            last_return = get_child(current_child)._execute(context)
            return ERR_BUSY
    elif child_count > current_child + 1:
        current_child += 1
        get_child(current_child)._open(context)
        last_return = get_child(current_child)._execute(context)
        return ERR_BUSY
    else:
        current_child = null
        last_return = null
        return OK 
