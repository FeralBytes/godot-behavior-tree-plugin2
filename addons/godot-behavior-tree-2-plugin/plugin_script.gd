tool

extends EditorPlugin

func _enter_tree():
    add_custom_type(
        "ActionBehaviorTreeNode",
        "Node",
        preload("res://addons/godot-behavior-tree-2-plugin/ActionBehaviorTree.gd"),
        preload("res://addons/godot-behavior-tree-2-plugin/action_icon.png")
    )
#
#    add_custom_type(
#        "BehaviorBlackboard",
#        "Node",
#        preload("res://addons/godot-behavior-tree-plugin/blackboard.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/blackboard_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorCondition",
#        "Node",
#        preload("res://addons/godot-behavior-tree-plugin/condition.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/condition_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorError",
#        "Node",
#        preload("res://addons/godot-behavior-tree-plugin/error.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/error_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorFailer",
#        "Node",
#        load("res://addons/godot-behavior-tree-plugin/failer.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/error_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorInverter",
#        "Node",
#        load("res://addons/godot-behavior-tree-plugin/inverter.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/inverter_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorLimiter",
#        "Node",
#        preload("res://addons/godot-behavior-tree-plugin/limiter.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/inverter_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorMaxLimiter",
#        "Node",
#        preload("res://addons/godot-behavior-tree-plugin/limiter.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/inverter_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorRepeater",
#        "Node",
#        load("res://addons/godot-behavior-tree-plugin/repeater.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/inverter_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorRepeatUntilFail",
#        "Node",
#        load("res://addons/godot-behavior-tree-plugin/repeat_until_fail.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/inverter_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorRepeatUntilSucceed",
#        "Node",
#        load("res://addons/godot-behavior-tree-plugin/repeat_until_succeed.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/inverter_icon.png")
#    )
#
    add_custom_type(
        "SelectorBehaviorTreeNode",
        "Node",
        preload("res://addons/godot-behavior-tree-2-plugin/SelectorBehaviorTree.gd"),
        preload("res://addons/godot-behavior-tree-2-plugin/selector_icon.png")
    )
#
#    add_custom_type(
#        "BehaviorMemSelector",
#        "Node",
#        preload("res://addons/godot-behavior-tree-plugin/mem_selector.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/mem_selector_icon.png")
#    )
#
    add_custom_type(
        "SequenceBehaviorTreeNode",
        "Node",
        preload("res://addons/godot-behavior-tree-2-plugin/SequenceBehaviorTree.gd"),
        preload("res://addons/godot-behavior-tree-2-plugin/sequence_icon.png")
    )
#
#    add_custom_type(
#        "BehaviorMemSequence",
#        "Node",
#        preload("res://addons/godot-behavior-tree-plugin/mem_sequence.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/mem_sequence_icon.png")
#    )
#
#    add_custom_type(
#        "BehaviorSucceeder",
#        "Node",
#        load("res://addons/godot-behavior-tree-plugin/succeeder.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/succeeder_icon.png")
#    )

    add_custom_type(
        "RootBehaviorTreeNode",
        "Node",
        preload("res://addons/godot-behavior-tree-2-plugin/RootBehaviorTree.gd"),
        preload("res://addons/godot-behavior-tree-2-plugin/root_icon.png")
    )

#    add_custom_type(
#        "BehaviorWait",
#        "Node",
#        preload("res://addons/godot-behavior-tree-plugin/wait.gd"),
#        preload("res://addons/godot-behavior-tree-plugin/action_icon.png")
#    )

func _exit_tree():
    remove_custom_type("ActionBehaviorTreeNode")
#    remove_custom_type("BehaviorBlackboard")
#    remove_custom_type("BehaviorCondition")
#    remove_custom_type("BehaviorError")
#    remove_custom_type("BehaviorFailer")
#    remove_custom_type("BehaviorInverter")
#    remove_custom_type("BehaviorLimiter")
#    remove_custom_type("BehaviorMaxTime")
#    remove_custom_type("BehaviorMaxLimiter")
#    remove_custom_type("BehaviorRepeater")
#    remove_custom_type("BehaviorRepeatUntilFail")
#    remove_custom_type("BehaviorRepeatUntilSucceed")
    remove_custom_type("SelectorBehaviorTreeNode")
#    remove_custom_type("BehaviorMemSelector")
    remove_custom_type("SequenceBehaviorTreeNode")
#    remove_custom_type("BehaviorMemSequence")
#    remove_custom_type("BehaviorSucceeder")
    remove_custom_type("RootBehaviorTreeNode")
#    remove_custom_type("BehaviorWait")
