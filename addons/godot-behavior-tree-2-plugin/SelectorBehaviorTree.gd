extends "res://addons/godot-behavior-tree-2-plugin/BaseBehaviorTree.gd"
# OK = 0 ERR_BUSY = 44 FAILED = 1
# Declare member variables here. Examples:
var current_child = null
var last_return = null
onready var child_count = get_child_count()

func print_if_debug(context):
    if context.debug:
        print("In Behavior Tree Selector. Last child return was = " + str(last_return) + " Current child is = " + str(current_child))

# Selector Compsite Node - iterates children until one returns OK
#   fails ONLY if all children fail (return FAILED)
func iteration(context):
    if last_return == OK:
        current_child = null
        last_return = null
        print_if_debug(context)
        return OK
    elif last_return == ERR_BUSY:
        last_return = get_child(current_child)._execute(context)
        print_if_debug(context)
        return ERR_BUSY
    if current_child == null:
        if child_count == 0:
            print_if_debug(context)
            return OK
        else:
            current_child = 0
            get_child(current_child)._open(context)
            last_return = get_child(current_child)._execute(context)
            print_if_debug(context)
            return ERR_BUSY
    elif child_count > current_child + 1:
        current_child += 1
        get_child(current_child)._open(context)
        last_return = get_child(current_child)._execute(context)
        print_if_debug(context)
        return ERR_BUSY
    else:
        current_child = null
        last_return = null
        print_if_debug(context)
        return FAILED
