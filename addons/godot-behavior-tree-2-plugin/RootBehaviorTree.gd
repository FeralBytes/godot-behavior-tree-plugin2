extends Node

# OK = 0 ERR_BUSY = 44 FAILED = 1
export var debug = false
export var only_every_ = 0  # Allows you to limit the AI processing to only every count worth of ticks.

class Context:
# warning-ignore:unused_class_variable
    var root = null
    var open_nodes = []
# warning-ignore:unused_class_variable
    var actor = null
# warning-ignore:unused_class_variable
    var memories = {}
# warning-ignore:unused_class_variable
    var debug 
# warning-ignore:unused_class_variable
    var only_every_ 
    
    func open_node(node):
        open_nodes.append(node)
        
    func close_node(node):
        open_nodes.erase(node)
        
var last_return = null
var only_every_counter = 0

onready var child = null
onready var child_count = get_child_count()
onready var context = Context.new()

func _ready():
    if child_count != 1:
        var msg = str("ERROR BehaviorTreeRoot node at ", get_name(), " should have one and only one child, use a Selector or Sequence.")
        print_debug(msg)
        get_tree().quit()
    else:
        child = get_child(0)
        context.root = self
        context.actor = get_parent()
        context.debug = debug
        context.only_every_ = only_every_
        
func _process(delta):
    if only_every_counter == context.only_every_:
        only_every_counter = 0
        iteration()
    else:
        only_every_counter += 1

# NOTE: Idea, I think we can still shortcut following the execute all of the way down. Perhaps if we 
# do context.open_nodes[-2]._execute(context). Can't do open_nodes[-1], because it will cause the parent 
# to miss the status update. Needs to be tested though, may not work. May also be a waste of time if 
# following execute down a long branch is nearly as fast.
func iteration():
    if child_count != 1:
        return ERR_CANT_ACQUIRE_RESOURCE
    if len(context.open_nodes) == 0:  # Start the tree traversal.
        child._open(context)
    last_return = child._execute(context)
    if context.debug:
        print("In Behavior Tree Root. Open Nodes = " + str(len(context.open_nodes)) + " Last return for tree = " + str(last_return))