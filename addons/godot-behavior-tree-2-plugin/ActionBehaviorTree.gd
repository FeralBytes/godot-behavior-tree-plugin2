extends "res://addons/godot-behavior-tree-2-plugin/BaseBehaviorTree.gd"
# OK = 0 ERR_BUSY = 44 FAILED = 1

var counter = 0
# Leaf Node
#warning-ignore:unused_variable
func iteration(context):
    if context.debug:
        print("In Behavior Tree Action.")
    counter += 1
    if counter == 2:
        counter = 0
        return OK
    else:
        return ERR_BUSY
